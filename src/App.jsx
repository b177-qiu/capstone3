import {Fragment, useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'
import AppNavbar from "./components/AppNavbar";
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error'
import Contact from './pages/Contact';
import AdminView from './components/AdminView';
import './App.css';
import {UserProvider} from './UserContext'

export default function App() {

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        fetch('https://fierce-plains-67179.herokuapp.com/users', {
            headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
        })
        .then(res => res.json())
        .then(data => {

            if (typeof data._id !== 'undefined') {
                setUser({ id: data._id, isAdmin: data.isAdmin });
            } else {
                setUser({ id: null, isAdmin: null });
            }
        })

    }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
            <AppNavbar />
            <Routes>
                <Route path="/" element={<Home />}/>
                <Route path="/register" element={<Register />}/>
                <Route path="/login" element={<Login />}/>
                <Route path="/products" element={<Products />}/>
                <Route path="/products/:productId" element={<ProductView />}/>
                <Route path="/admin" element={<AdminView />}/>
                <Route path="/logout" element={<Logout />}/>
                <Route path="/contact" element={<Contact />}/>
                <Route path="*" element={<Error />}/>
            </Routes>
        </Router>
    </UserProvider>
  );
}
