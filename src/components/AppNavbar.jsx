import React, { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

    const { user } = useContext(UserContext);

    return (
        <Navbar  expand="lg">
            <Nav className="logo">Modern Bookshelf</Nav>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={Link} to="/" className="nav">Home</Nav.Link>
                <Nav.Link as={Link} to="/products" className="nav">Shop</Nav.Link>
                {(function(){
                        if(user.id !== null && user.isAdmin === false){
                            return (
                            <React.Fragment>
                                <Nav.Link as={Link} to="/logout" className="nav" exact>Logout</Nav.Link>
                            </React.Fragment>
                            )
                        } else if(user.id !== null && user.isAdmin === true){
                            return (
                            <React.Fragment>
                                <Nav.Link as={Link} to="/admin" className="nav" exact>Admin</Nav.Link>
                                <Nav.Link as={Link} to="/logout" className="nav" exact>Logout</Nav.Link>
                            </React.Fragment>
                            )
                        } else {
                            return (
                            <React.Fragment>
                                <Nav.Link as={Link} to="/login" className="nav" exact>Login</Nav.Link>
                                <Nav.Link as={Link} to="/register" className="nav" exact>Register</Nav.Link>
                            </React.Fragment>
                            )
                        }
                    })()}

                <Nav.Link as={Link} to="/contact" className="nav">Contact Us</Nav.Link>
              </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
