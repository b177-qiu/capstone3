// import Button from 'react-bootstrap/Button';
// // Boottstrap grid system components
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel';

export default function Banner(){

    return (
        <Carousel fade>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://res.cloudinary.com/dgjzjcock/image/upload/v1657447880/books/banner1_czqbbj.jpg"
              alt="First slide"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://res.cloudinary.com/dgjzjcock/image/upload/v1657542920/books/banner2_oxhyut.jpg"
              alt="Second slide"
            />
          </Carousel.Item>
        </Carousel>
    )
}
