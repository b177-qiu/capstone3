import React, { useContext, useEffect, useState } from "react";
import { Container, Card, Button, Row } from "react-bootstrap";
import { Link, useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function ProductView(){

    const { user } = useContext(UserContext);
    const history = useNavigate();

    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    // const [id, setId] = useState("")

    const addCart = (productId) => {
        fetch('https://fierce-plains-67179.herokuapp.com/users/myOrders', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data){
                Swal.fire({
                    title: "Successfully added to cart",
                    icon: "success",
                    text: "You have successfully added to cart."
                })
                history.push("/products");
            }
            else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })
    }

    // const fetchProduct = (productId) =>{
    // fetch(`https://fierce-plains-67179.herokuapp.com/products/${productId}`)
    // .then(res => res.json())
    // .then(data => {

    //     setId(productId);
    //     setName(data.name);
    //     setDescription(data.description);
    //     setPrice(data.price);
    // })}

    useEffect(() => {
        fetch(`https://fierce-plains-67179.herokuapp.com/products/${productId}`)
            .then(res => res.json())
            .then(data => {

                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
            })
    }, [productId]);

    return(
        <Container>
            <Row>
            <Card style={{ width: '18rem' }}>
                <Card.Body className="text-center">
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle className="mb-2 text-muted">Price</Card.Subtitle>
                    <Card.Text>Php {price}</Card.Text>
                    {user.id !== null ?
                       <Button variant="primary" onClick={() => addCart(productId)} block>Add to Cart</Button>
                        :
                        <Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
                    }
                    
                </Card.Body>
            </Card>
            </Row>
        </Container>
    )
}
