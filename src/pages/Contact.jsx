import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ContactUs(){

    const {user} = useContext(UserContext);
    const history = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    console.log(email);

    return (

  <Row className="justify-content-center">
    <Col xs md="6">  
      <h1 className = "name text-center my-4">Contact Us</h1>
      <Card className = 'p-3'>
        <Form onSubmit={(e) => ContactUs(e)}>
          <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control 
                  type="text" 
                  placeholder="Enter first name"
                  value={firstName} 
                  onChange={e => setFirstName(e.target.value)}
                  required
              />
          </Form.Group>

          <Form.Group controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control 
                  type="text" 
                  placeholder="Enter last name"
                  value={lastName} 
                  onChange={e => setLastName(e.target.value)}
                  required
              />
          </Form.Group>


          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email" 
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Message</Form.Label>
            <Form.Control 
            	className = "message"
            	size = "lg"
                type="textarea" 
                placeholder="Message" 
                value={message}
                onChange={e => setMessage(e.target.value)}
                required
            />
          </Form.Group>
                <Button variant="primary" type="submit" id="submitBtn">
                  Submit
                </Button>
          
        </Form>
      </Card>
    </Col>              
 </Row>
    )
}
