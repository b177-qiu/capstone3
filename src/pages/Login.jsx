import React, { useState, useEffect ,useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { Link, Redirect, useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function LoginForm(props){

    const history = useNavigate();
    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [ isActive, setIsActive ] = useState(true);
    const [willRedirect, setWillRedirect] = useState(false);

    const authenticate = (e) => {

        e.preventDefault();

        fetch('https://fierce-plains-67179.herokuapp.com/users/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if (typeof data.access !== 'undefined') {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);
                Swal.fire({
                    title: 'Login Successful',
                    icon: 'success',
                })

            } else {
                Swal.fire({
                    title: 'Authentication Failed',
                    icon: 'error',
                    text: 'Authentication failed. Please check your login details and try again'
                })
            }

        })

            setEmail('');
            setPassword('');

    }
    
    const retrieveUserDetails = (token) => {

        fetch('https://fierce-plains-67179.herokuapp.com/users/details', {
            headers: { Authorization: `Bearer ${ token }`}
        })
        .then(res => res.json())
        .then(data => {

            setUser({ id: data._id, isAdmin: data.isAdmin });

            if (data.isAdmin === true) {
                setWillRedirect(true);
            } else {
                if (props.location.state.from === 'cart') {
                    history.goBack();
                } else {
                    setWillRedirect(true);
                }
            }
        })
    }
    useEffect(() => {

            if(email !== '' && password !== ''){
                setIsActive(true)
            }
            else{
                setIsActive(false)
            }
        }, [email, password]);

    return(
        function(){
            if(user.id !== null && user.isAdmin === false){
                return (
                    <Navigate to="/products" />
                )
            } else if(willRedirect === true && user.isAdmin === true){
                return (
                    <Navigate to="/adminView" />
                )
            } else {
                return(
                    <Row className="justify-content-center">
                        <Col xs md="6">
                            <h2 className="text-center my-4 name">Log In</h2>
                            <Card>
                                <Form onSubmit={e => authenticate(e)}>
                                    <Card.Body>
                                        <Form.Group controlId="userEmail">
                                            <Form.Label>Email:</Form.Label>
                                            <Form.Control
                                                type="email"
                                                placeholder="Enter your email"
                                                value={email}
                                                onChange={e => setEmail(e.target.value)}
                                                required
                                            />
                                        </Form.Group>

                                        <Form.Group controlId="password">
                                            <Form.Label>Password:</Form.Label>
                                            <Form.Control
                                                type="password"
                                                placeholder="Enter your password"
                                                value={password}
                                                onChange={e => setPassword(e.target.value)}
                                                required
                                            />
                                        </Form.Group>

                                    </Card.Body>
                                    <Card.Footer>
                                        <Button variant="primary" type="submit" block >
                                            Submit
                                        </Button>
                                    </Card.Footer>
                                </Form>
                            </Card>
                            <p className="text-center mt-3">
                                Don't have an account yet? <Link to="/register">Click here</Link> to register.
                            </p>
                        </Col>              
                    </Row>
                )
            }
        }()  
    )   
}

