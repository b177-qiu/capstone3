import React, { Fragment, useEffect, useState , useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products(){

	const {user} = useContext(UserContext);

	const [products, setProducts] = useState([]);
	useEffect(() => {
		fetch('https://fierce-plains-67179.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
					return (
						<ProductCard key = {product._id} productProp={product} />
					);
				})
			)
		})
	}, []);

	return(
		<Fragment>
			{products}
		</Fragment>
	)
}
