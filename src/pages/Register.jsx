import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button,Card, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

    const {user} = useContext(UserContext);
    const history = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    function registerUser(e){
        e.preventDefault();

        fetch('https://fierce-plains-67179.herokuapp.com/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

                if(data === true){
                    Swal.fire({
                        title: 'Duplicate email found',
                        icon: 'error',
                        text: 'Kindly provide another email to complete the registration.'  
                    });
                }else {

                        fetch('https://fierce-plains-67179.herokuapp.com/users/register', {
                            method: "POST",
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                firstName: firstName,
                                lastName: lastName,
                                email: email,
                                mobileNo: mobileNo,
                                password: password1
                            })
                        })
                        .then(res => res.json())
                        .then(data => {

                            if (data === true) {

                                setFirstName('');
                                setLastName('');
                                setEmail('');
                                setMobileNo('');
                                setPassword1('');
                                setPassword2('');

                                Swal.fire({
                                    title: 'Registration successful',
                                    icon: 'success',
                                    text: 'Welcome to Zuitt!'
                                });

                                history("/login");

                            } else {

                                Swal.fire({
                                    title: 'Something wrong',
                                    icon: 'error',
                                    text: 'Please try again.'   
                                });

                            }
                        })
                    };
        })

        setEmail('');
        setPassword1('');
        setPassword2('');

    }

    useEffect(() => {
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNo,email, password1, password2]);

    return (
        (user.id !== null) ?
            <Navigate to="/products" />
        :
   <Row className="justify-content-center">
    <Col xs md="6">  
        <h1 className = "name text-center my-4">Register</h1>
        <Card className = "p-3">   
            <Form onSubmit={(e) => registerUser(e)}>

          <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control 
                  type="text" 
                  placeholder="Enter first name"
                  value={firstName} 
                  onChange={e => setFirstName(e.target.value)}
                  required
              />
          </Form.Group>

          <Form.Group controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control 
                  type="text" 
                  placeholder="Enter last name"
                  value={lastName} 
                  onChange={e => setLastName(e.target.value)}
                  required
              />
          </Form.Group>


          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email" 
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="mobileNo">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control 
                  type="text" 
                  placeholder="Enter Mobile Number"
                  value={mobileNo} 
                  onChange={e => setMobileNo(e.target.value)}
                  required
              />
          </Form.Group>


          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password1}
                onChange={e => setPassword1(e.target.value)}
                required
            />
          </Form.Group>
          
          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Verify Password" 
                value={password2}
                onChange={e => setPassword2(e.target.value)}
                required
            />
          </Form.Group>
          {/* Conditionally render submit button based on isActive state */}
          { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                  Submit
                </Button>
            :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                  Submit
                </Button>
          }
          
        </Form>
        </Card> 
    </Col>              
 </Row>
    )
}
